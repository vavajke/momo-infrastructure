# Momo-store Infrastructure

## Terraform
Инфраструктура для приложения описана в файле `main.tf` для использования с помощью Terraform. Состояние Terraform хранится в s3.
Кластер состоит из трёх нод.

---

## Helm приложения
Для разворачивания приложения в k8s используется helm чарт, в котором описано всё, что необходимо для работы frontend и backend части приложения. Пакет можно посмотреть по урлу https://nexus.praktikum-services.ru/#browse/browse:momo-store-maysak:momo-store.

Репозиторий: https://nexus.praktikum-services.ru/repository/momo-store-maysak/

Пакет: `momo-store-chart`

Для удобства релиза чарта в репозиторий создан bash-скрипт `helm.upload.sh`. Для его работы требуется `.env` файл.

### Ingress
Для правильного роутинга применен Ingress с правилами:

- `/api/`: ведет на backend с правилом rewrite на корень
- `/`: все остальные запросы идут на frontend

### Deploy
Деплой приложения описан в `.gitlab-ci.yml` с единственным шагом деплоя. Для успешного деалоя нужно не забыть добавить необходимы переменные в CI/CD Variables gitlab. Переменные `K8S_CONFIG`, `VALUES_YAML` и `NEXUS_PASSWORD` должны быть заданы в base64 формате.

Деплой запускается только на `master` ветке.

---

## Helm мониторинга

В качестве мониторинга приложения используется набор инструментов:
- kube-state-metrics
- Promtail
- Loki
- Prometheus
- Grafana

## Loki
Приёмник логов. Доступен по урлу [loki.pelmenster.ru](https://loki.pelmenster.ru/). Готов для забора логов.

## Prometheus
Сборщик метрик. Доступен по урлу [prometheus.pelmenster.ru](https://prometheus.pelmenster.ru/). 

Сконфигурирован на сборку следующих метрик:
- endpoints
- node
- pod
- cadvisor

## Grafana
Инструмент мониторинга и алертинга. Доступен по урлу [grafana.pelmenster.ru](https://grafana.pelmenster.ru/).

Доступ:
- Логин: admin
- Пароль: h2Nsoc20KrX1LfeokMN9GZrMjhXwGVS01KniT4cl

Связан с Loki и Prometheus.

### Dashboards

Стандартные дашборды уже присутствует в системе и позволяет наблюдать за ресурсами системы `CPU, Memory, IO, Filesysyem`, за сетевой активностью `NGINX Ingress controller` и за метриками приложения `Momo-store metrics`.

### Alerts

Система обогащена алертами на различные критические события:
- Истощение ресурсов памяти, cpu, файловой системы
- Коды ошибок backend 500

Алертинг настроен на канал связи Telegram.
