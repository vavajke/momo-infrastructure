#!/bin/bash
export $(cat .env | xargs)

if [ ! -z "${NEXUS_LOGIN}" ] && [ ! -z "${NEXUS_PASSWORD}" ]
then
    package_result=$(helm package ./momo-store-chart)
    package_file="${package_result##*/}"
    curl -u ${NEXUS_LOGIN}:${NEXUS_PASSWORD} https://nexus.praktikum-services.ru/repository/momo-store-maysak/ --upload-file ${package_file}
    echo "Loaded"
else
    echo "Make .env file"
fi