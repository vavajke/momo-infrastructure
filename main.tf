terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "maysak-momo-store"
    region     = "ru-central1"
    key        = "terraform/terraform.tfstate"
    access_key = "YCAJEM9_bWs4TtykJz3OjzUEI"
    secret_key = "YCOudsgq8Ry7ITHpAoodXpk_Nb2-Oras1O_lHawf"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = "AQAAAAABLjRCAATuwX-yFgSw00XKu7jUMhvIiGI"
  cloud_id  = "b1gdr7s2du8mgol4q5pm"
  folder_id = "b1gc1839hep1bqsdalu8"
  zone      = "ru-central1-a"
}

resource "yandex_kubernetes_cluster" "momo-store" {
  name        = "momo-store"
  description = "Кластер momo-store магазина"
  network_id  = yandex_vpc_network.momo-network.id

  master {
    regional {
      region = "ru-central1"

      location {
        zone      = yandex_vpc_subnet.momo-network-ru-central1-a.zone
        subnet_id = yandex_vpc_subnet.momo-network-ru-central1-a.id
      }

      location {
        zone      = yandex_vpc_subnet.momo-network-ru-central1-b.zone
        subnet_id = yandex_vpc_subnet.momo-network-ru-central1-b.id
      }

      location {
        zone      = yandex_vpc_subnet.momo-network-ru-central1-c.zone
        subnet_id = yandex_vpc_subnet.momo-network-ru-central1-c.id
      }
    }

    version   = "1.20"
    public_ip = true

    maintenance_policy {
      auto_upgrade = true
    }
  }

  service_account_id      = yandex_iam_service_account.momo-terraform.id
  node_service_account_id = yandex_iam_service_account.momo-terraform.id
  depends_on = [
      yandex_resourcemanager_folder_iam_binding.editor,
      yandex_resourcemanager_folder_iam_binding.images-puller
  ]

  release_channel = "REGULAR"
}

resource "yandex_kubernetes_node_group" "momo-store" {
  cluster_id  = yandex_kubernetes_cluster.momo-store.id
  name        = "momo-store"
  description = "Группа узлов для кластера momo-store"
  version     = "1.20"
  
  instance_template {
    platform_id = "standard-v1"
    
    network_interface {
      nat                = true
      subnet_ids         = [
        yandex_vpc_subnet.momo-network-ru-central1-a.id, 
        yandex_vpc_subnet.momo-network-ru-central1-b.id,
        yandex_vpc_subnet.momo-network-ru-central1-c.id
      ]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "docker"
    }
  }
  
  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  allocation_policy {
    location {
      zone      = yandex_vpc_subnet.momo-network-ru-central1-a.zone
    }

    location {
      zone      = yandex_vpc_subnet.momo-network-ru-central1-b.zone
    }

    location {
      zone      = yandex_vpc_subnet.momo-network-ru-central1-c.zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true
  }
}

resource "yandex_vpc_network" "momo-network" { 
    name = "momo-network" 
}

resource "yandex_vpc_subnet" "momo-network-ru-central1-a" {
 v4_cidr_blocks = ["10.128.0.0/24"]
 zone           = "ru-central1-a"
 network_id     = yandex_vpc_network.momo-network.id
}

resource "yandex_vpc_subnet" "momo-network-ru-central1-b" {
 v4_cidr_blocks = ["10.129.0.0/24"]
 zone           = "ru-central1-b"
 network_id     = yandex_vpc_network.momo-network.id
}

resource "yandex_vpc_subnet" "momo-network-ru-central1-c" {
 v4_cidr_blocks = ["10.130.0.0/24"]
 zone           = "ru-central1-c"
 network_id     = yandex_vpc_network.momo-network.id
}

resource "yandex_iam_service_account" "momo-terraform" {
 name        = "momo-terraform"
 description = "Для управления кластером с terraform"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 folder_id = "b1gc1839hep1bqsdalu8"
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.momo-terraform.id}"
 ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
 folder_id = "b1gc1839hep1bqsdalu8"
 role      = "container-registry.images.puller"
 members   = [
   "serviceAccount:${yandex_iam_service_account.momo-terraform.id}"
 ]
}