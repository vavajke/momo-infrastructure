{{- define "common_labels" }}
app: {{ .Chart.Name }}
app.kubernetes.io/name: "{{ .Release.Name }}-{{ .Chart.Name }}"
app.kubernetes.io/instance: "{{ .Release.Name }}"
app.kubernetes.io/version: "{{ .Chart.AppVersion }}"
app.kubernetes.io/managed-by: "{{ .Release.Service }}"
helm.sh/chart: "{{ .Release.Name }}-{{ .Chart.Name }}-{{ .Chart.Version }}"
app.kubernetes.io/component: {{ .Chart.Name }}
app.kubernetes.io/part-of: {{ .Release.Name }}
env: "{{ .Values.global.environment }}" 
{{- end }}